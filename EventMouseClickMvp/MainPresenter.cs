﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventMouseClickMvp
{
    public class MainPresenter
    {
        private readonly IMainView _mainView;

        public MainPresenter(IMainView mainView)
        {
            _mainView = mainView ?? throw new ArgumentNullException(nameof(mainView));

            _mainView.ClickOK = new SimpleButtonEventHandler(Do);
            _mainView.TextChange = new SimpleButtonEventHandler(Do, _mainView);
        }

        private void Do()
        {
        }
    }
}
