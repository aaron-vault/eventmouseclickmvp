﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventMouseClickMvp
{
    public interface IMainView
    {
        SimpleButtonEventHandler ClickOK { get; set; }
        SimpleButtonEventHandler TextChange { get; set; }
        string LabelText { get; set; }
        string TextBoxText { get; }
    }

    public partial class Form1 : Form, IMainView
    {
        private BindingSource _bsButtonClick = new BindingSource();
        private BindingSource _txtChange = new BindingSource();

        public Form1()
        {
            InitializeComponent();

            _bsButtonClick.DataSource = typeof(SimpleButtonEventHandler);
            button1.DataBindings.Add("", _bsButtonClick, "");

            _txtChange.DataSource = typeof(SimpleButtonEventHandler);
            textBox1.DataBindings.Add("", _txtChange, "");
        }

        public string LabelText
        {
            get => lblText.Text;
            set
            {
                lblText.Text = value;
            }
        }

        public string TextBoxText
        {
            get => textBox1.Text;
        }

        public SimpleButtonEventHandler ClickOK
        {
            get => _bsButtonClick.Current as SimpleButtonEventHandler;
            set
            {
                if (_bsButtonClick.Count > 0) return;
                _bsButtonClick.Add(value);
                button1.Click += value.Handler;
            }
        }

        public SimpleButtonEventHandler TextChange
        {
            get => _txtChange.Current as SimpleButtonEventHandler;
            set
            {
                if (_txtChange.Count > 0) return;
                _txtChange.Add(value);
                textBox1.TextChanged += value.LabelText;
            }
        }
    }
}
