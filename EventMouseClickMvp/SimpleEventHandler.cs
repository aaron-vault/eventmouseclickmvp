﻿using System;
using System.Windows.Forms;

namespace EventMouseClickMvp
{
    public class SimpleEventHandler
    {
        private Action _handlerExecutor;

        //ctor
        public SimpleEventHandler(Action handlerExecutor)
        {
            _handlerExecutor = handlerExecutor ??
                throw new ArgumentNullException(nameof(handlerExecutor));

            Handler = new EventHandler(OnEvent);
            Change = new EventHandler(TextChange);
        }

        /// <summary>
        /// Ссылка на метод,
        /// который буден вызван при возникновении события у целевого контрола 
        /// </summary>
        public EventHandler Handler { get; private set; }
        public EventHandler Change { get; private set; }

        /// <summary>
        /// Вызов по событию исполняемого метода
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnEvent(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }

        private void TextChange(object sender, EventArgs e)
        {
            
        }
    }
}