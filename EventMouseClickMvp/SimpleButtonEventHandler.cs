﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EventMouseClickMvp
{
    public class SimpleButtonEventHandler : SimpleEventHandler, INotifyPropertyChanged
    {
        private Action _handlerExecutor;
        private IMainView _view;

        public EventHandler LabelText { get; private set; }

        public SimpleButtonEventHandler(Action handlerExecutor) : base(handlerExecutor)
        {
        }

        public SimpleButtonEventHandler(Action handlerExecutor, IMainView view) : base(handlerExecutor)
        {
            _handlerExecutor = handlerExecutor;
            _view = view;

            LabelText = new EventHandler(ChangeLabelText);
        }

        public void ChangeLabelText(object sender, EventArgs e)
        {
            _view.LabelText = _view.TextBoxText;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}